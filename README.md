PPW KC06
----
 
Anggota kelompok : 
----
- Farrel Abi S
- Lubna Syifa
- Mario Ekaputta
- Muhammad Glend Angga Dwitama

Latar belakang :
----
Majalah dinding adalah tempat mahasiswa untuk mendapatkan informasi, seperti perlombaan, pengumuman, seminar, magang, beasiswa, dan lain sebagainya. Dulu, majalah dinding dapat dilihat di berbagai macam lokasi strategis yang telah disediakan oleh pihak fakultas. Namun dengan berkembangnya zaman, semua informasi kini dapat dilihat dimanapun dan kapanpun. Begitupula dengan majalah dinding, sudah seharusnya dapat diakses dalam bentuk digital. Oleh karena itu, cspost adalah sebuah jawaban dari permasalahan tersebut.

CSPost adalah sebuah website yang berisi berbagai macam informasi dengan target pembaca adalah mahasiswa Fakultas Ilmu Komputer UI. CSPost dapat memberikan sugesti bagi penggunanya atas apa yang lebih ia sering cari di web CSPost. Tentunya untuk mengetahui habit atau kebiasaan pengguna, para pengunjung diharuskan untuk melakukan login terlebih dahulu atau melakukan pendaftaran akun apabila belum terdaftar dalam database website.

Daftar fitur :
----
- Home
- Lomba
- Seminar /  talkshow
- Organisasi
- Login / Signup
- Search

Spesifikasi fitur:
- Home (Abi)
- Search
- Events List/Rekomendasi
- Detail Event (Mario)

- Create Event (Glend)
Views upload to database
- Register/Login(Lubna)
Model User
Link herokuapp : 
https://cspost.herokuapp.com/
----

[![pipeline status](https://gitlab.com/ppw-kc06/cspost/badges/master/pipeline.svg)](https://gitlab.com/ppw-kc06/cspost/commits/master)