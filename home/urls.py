from django.urls import path
from home import views

urlpatterns = [
    path('',views.home,name='index'),
    path('faq/', views.faq,name='faq'),
]