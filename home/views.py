from django.shortcuts import render,redirect

from django.db.models import Q
from post.models import Post

def home(request):
    # object_list = Post.objects.all()
    # context = {'table_list' : object_list}
    # query = request.GET.get('q')
    # if(query):
    #     object_list = Post.objects.filter(
    #         Q(nama_Kegiatan__icontains=query) | Q(deskripsi__icontains=query)
    #     )
    #     context['table_list'] = object_list
    # filterkategori = request.GET.get('filter')
    # if(filterkategori):
    #     if(filterkategori=='organisasi'):
    #        context['table_list'] = Post.objects.filter(Organisasi=True)
    #     elif(filterkategori=='seminar'):
    #         context['table_list'] = Post.objects.filter(Seminar = True)
    #     elif(filterkategori=='lomba'):
    #         context['table_list']  = Post.objects.filter(Lomba = True)
    return render(request,'pages/index.html')

def faq(request):
    return render(request,'pages/faq.html')
