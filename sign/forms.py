from django import forms
from sign.models import UserInfo
from django.contrib.auth.models import User

class UserForm(forms.Form):
    username = forms.CharField(widget=forms.TextInput(attrs={'class':'form-control login-input'}))
    password = forms.CharField(widget=forms.PasswordInput(attrs={'class':'form-control login-input'}))

class RegisterForm(forms.ModelForm):
    username = forms.CharField(widget=forms.TextInput(attrs={'class':'form-control'}))
    password = forms.CharField(widget=forms.PasswordInput(attrs={'class':'form-control'}))
    email = forms.CharField(widget=forms.EmailInput(attrs={'class':'form-control'})) 
    
    class Meta():
        model = User
        fields = ('username', 'password','email')