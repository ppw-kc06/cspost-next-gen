from django.urls import path
from sign import views

urlpatterns = [
    path('',views.login,name='login'),
    path('logout/',views.logout, name='logout'),
    path('register/',views.register,name='register'),
]