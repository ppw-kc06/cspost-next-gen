from django.shortcuts import render,redirect
from sign.forms import UserForm, RegisterForm
from django.contrib.auth import authenticate, logout as logout_function
from django.contrib.auth.models import User, UserManager
from django.contrib.auth import login as login_function
from django.http import HttpResponseRedirect, HttpResponse
from django.urls import reverse
from django.contrib.auth.decorators import login_required

def logout(request):
    logout_function(request)
    return HttpResponseRedirect(reverse('index'))

def register(request):
	if request.method == "POST":
		form = RegisterForm(request.POST)
		if form.is_valid():
			user = form.save()
			user.set_password(request.POST['password'])
			user.save()
			print(user)
			new_user = authenticate(request, username=form.cleaned_data['username'], password=form.cleaned_data['password'])
			login_function(request, new_user)
			return HttpResponseRedirect(reverse('index'))
		return render(request, "pages/index.html", {"form":form})
	else:
		form = RegisterForm()
	return render(request, "pages/register.html", {"form":form})

def login(request):
	if request.method == 'POST':
		form = UserForm(request.POST)
		print(request.POST)
		user = authenticate(request, username = request.POST['username'], password = request.POST['password'])	
		if user is not None:
			login_function(request, user)
			return HttpResponseRedirect(reverse('index'))
		else:
			return HttpResponse("please try again!")
	else:
		form = UserForm()
	return render(request, 'pages/login.html', {'form': form})