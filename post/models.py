from django.db import models

# Create your models here.


class Post(models.Model):
    nama_Kegiatan = models.CharField(max_length = 20)
    Lomba = models.BooleanField()
    Organisasi = models.BooleanField()
    Seminar = models.BooleanField()
    tanggal = models.DateField()
    lokasi = models.CharField(max_length = 20)
    contact = models.TextField()
    deskripsi = models.TextField()
    syarat_dan_ketentuan = models.TextField()
