from django.urls import path
from . import views


urlpatterns = [
    path('createpost/',views.post,name='postevent'),
    path('datajson',views.json,name='json'),
    path('',views.home,name='index'),
]