const myUrl = "/post/datajson"
    $(document).ready(() => {
      $.ajax({
        method: 'GET',
        url: myUrl,
        success: function(res){
            $("#search-result").empty();
            semuaPost(res)
        }
    })

      $('#pencarian').submit(function (event) {
          event.preventDefault();
          const eventTitle = $('#q').val();
          if(eventTitle!=""){
              $.ajax({
              method: 'GET',
              url: myUrl+"?q="+eventTitle,
              success: function (result) {
                formatSearchRecommendation(result,eventTitle)
                
                  
              }
          });
      }
    })
    });
    const listPost = (post) => {
      let searchRow = $('#search-result');
                    searchRow.empty();
                    searchRow.append(
                      `<div class='col-lg-3 col-md-4 col-6'>
                        <a href= "/event/details/${post.pk}" class="d-block mb-4 h-100">
                        <p class='event-title' >${post.fields.nama_Kegiatan}</p>
                        </a>
                        <p class='event-title'>${post.fields.tanggal}</p>
                        <p class='event-title'>${post.fields.lokasi}</p>
                      </div>`
                    );
    }
    const semuaPost = (data) => {  
    data.forEach(function(post, index){
        listPost(post)
    })
}
const formatSearchRecommendation = (data, key) => {
    data.forEach(function(post, index){
        if(post.fields.nama_Kegiatan.toLowerCase().includes(key.toLowerCase()) || post.fields.deskripsi.toLowerCase().includes(key.toLowerCase())){
          listPost(post);
        }  
    })
}


$("#themeSwitch").on("click", () => {
    if ($("#themeSwitch").is(":checked")) {
      $(":root").css("--background-color", "rgba(8, 6, 6, 0.836)");
      $(":root").css("--dark-background-color", "rgba(250, 244, 244, 0.959)");
      $(":root").css("--text-color", "rgba(250, 244, 244, 0.959)");
    } else {
      $(":root").css("--background-color", "rgba(250, 244, 244, 0.959)");
      $(":root").css("--dark-background-color", "rgba(8, 6, 6, 0.836)");
      $(":root").css("--text-color", "rgba(8, 6, 6, 0.836)");
  }
  });

  $(document).ready(function(){
    $("p").click(function(){
      alert("This web is made by PPWKC06");
    });
  });

  $(".accordion_tab").click(function(){
    if ($(this).parent().hasClass("active")){
      $(this).parent().removeClass("active");
      $(this).siblings(".accordion_content").slideUp(400);
    }
    else{ 
      $(this).parent().addClass("active");
      $(this).parent().siblings().removeClass("active");
      $(".accordion_tab").siblings(".accordion_content").slideUp(400);
      $(this).siblings(".accordion_content").slideDown(400);
    }
  })